import collections
import fnmatch
import itertools
import lzma
import ntpath
import os


def ncd_lzma(filex, filey):
    xbytes = open(filex, 'rb').read()
    ybytes = open(filey, 'rb').read()
    xybytes = xbytes + ybytes
    cx = lzma.compress(xbytes)
    print("compressed length of %s is %d"%(filex, len(cx)))
    cy = lzma.compress(ybytes)
    print("compressed length of %s is %d"%(filey, len(cy)))
    cxy = lzma.compress(xybytes)
    print("compressed length of compressed file is %d"%(len(cxy)))
    if len(cy) > len(cx):
        n = (len(cxy) - len(cx)) / float(len(cy))
    else:
        n = (len(cxy) - len(cy)) / float(len(cx))
    return n

def cartesian(arrays, out=None):
    """
    slurped from http://stackoverflow.com/questions/1208118/using-numpy-to-build-an-array-of-all-combinations-of-two-arrays

    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out

def getFileList(rootdir, filePattern):
    FileList = [os.path.join(dirpath, f) \
            for dirpath, dirnames, files in os.walk(rootdir) \
            for f in fnmatch.filter(files, filePattern)]
    return FileList


def getPairs(someList):
    return itertools.product(someList, repeat=2)

def getAllPools(pool_list, min_pool_size=2):
    return itertools.chain.from_iterable(itertools.combinations(pool_list, r) \
            for r in range(min_pool_size, 3))
            # for r in range(min_pool_size, len(pool_list) + 1))


def writeResults(FileList, outputFile, compressionType):
    all_distances = collections.defaultdict(dict)
    with open(outputFile, 'w') as f:
        for pool in getAllPools(FileList):
            for f1, f2 in itertools.combinations(pool, 2):
                file1 = ntpath.basename(f1)
                file2 = ntpath.basename(f2)
                distance = compressionType(f1, f2)
                print("%s <--> %s : %s"%(file1, file2, distance))
                f.write("%s , %s , %s%s"%(file1, file2, distance,os.linesep))
                all_distances[file1][file2] = distance
        print("Number of files pairwise matched = %d"%(len(all_distances.keys())))

if __name__ == "__main__":
    import sys
    if not len(sys.argv) > 1:
        dirPath = os.path.dirname(os.path.abspath(__file__))
        print(os.path.dirname(os.path.abspath(__file__)))
    else:
        dirPath = sys.argv[1]

    FileList = getFileList(dirPath, '*')
    print("Number of files: %d"%(len(FileList)))
    outputFile = "NCD_OutputFile"
    writeResults(FileList, outputFile, ncd_lzma)
