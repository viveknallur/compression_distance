import collections
import fnmatch
import itertools
import lzma
import ntpath
import os


def calc_lzma_write(filex):
    xbytes = open(filex, 'rb').read()
    cx = lzma.compress(xbytes)
    comp_filex = ''.join([filex, ".cz"])
    with open(comp_filex, 'wb') as czf:
        czf.write(cx)
    return comp_filex

def getFileList(rootdir, filePattern):
    FileList = [os.path.join(dirpath, f) \
            for dirpath, dirnames, files in os.walk(rootdir) \
            for f in fnmatch.filter(files, filePattern)]
    return FileList


def writeResults(FileList, compressionType):
    for f1 in FileList:
        print("Processing file: %s"%(f1))
        compressed_file = compressionType(f1)
        print("Wrote compressed file: %s"%(compressed_file))

if __name__ == "__main__":
    import sys
    if not len(sys.argv) > 1:
        dirPath = os.path.dirname(os.path.abspath(__file__))
        print(os.path.dirname(os.path.abspath(__file__)))
    else:
        dirPath = sys.argv[1]

    FileList = getFileList(dirPath, '*')
    print("Number of files: %d"%(len(FileList)))
    writeResults(FileList, calc_lzma_write)
