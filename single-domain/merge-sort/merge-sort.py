from heapq import merge

def mergesort(w):
    """Sort list w and return it."""
    if len(w)<2:
        return w
    else:    
        mid=len(w)//2
        # sort the two halves of list w recursively with mergesort and merge them
        return merge(mergesort(w[:mid]), mergesort(w[mid:]))

