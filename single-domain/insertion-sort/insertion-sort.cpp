#include <algorithm>

template<class Iterator>
void insertion_sort( Iterator a, Iterator end )
{
    std::iter_swap( a, std::min_element( a, end ) );
 
    for ( Iterator b = a; ++b < end; a = b )
        for( Iterator c = b; *c < *a; --c, --a )
            std::iter_swap( a, c );
}

