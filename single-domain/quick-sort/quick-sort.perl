sub qsort {
  return () unless @_;
  (qsort(grep { $_ < $_[0] } @_[1..$#_]), $_[0],
   qsort(grep { $_ >= $_[0] } @_[1..$#_]));
}
