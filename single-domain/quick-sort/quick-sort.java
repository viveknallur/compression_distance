import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public static <T> List<T> Quicksort(List<T> v, BiFunction<T, T, Integer> comparer)
{
        if (v.size() < 2)
                return v;
        
        T pivot = v.get(v.size() / 2);

        List<T> l = new LinkedList<T>(Quicksort(v.stream().filter(x -> comparer.apply(x, pivot) < 0).collect(Collectors.toList()), comparer));
        l.addAll( v.stream().filter(x -> comparer.apply(x, pivot) == 0).collect(Collectors.toList()) );
        l.addAll( Quicksort(v.stream().filter(x -> comparer.apply(x, pivot) > 0).collect(Collectors.toList()), comparer) );
        
        return l;
}
