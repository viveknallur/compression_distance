
void quick(int array[], int start, int end){
    if(start < end){
        int l=start+1, r=end, p = array[start];
        while(l<r){
            if(array[l] <= p)
                l++;
            else if(array[r] >= p)
                r--;
            else
                swap(array[l],array[r]);
        }
        if(array[l] < p){
            swap(array[l],array[start]);
            l--;
        }
        else{
            l--;
            swap(array[l],array[start]);
        }
        quick(array, start, l);
        quick(array, r, end);
    }
}
