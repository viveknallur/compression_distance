import heapq
def heapsort(lst):
    # Copy the list into a temporary list
    heap = list(lst)

    # Make it into a heap
    heapq.heapify(heap)
    
    # Elements come off the heap in ascending order
    for i in xrange(len(lst)):
        lst[i] = heapq.heappop(heap)
