void selectionSort(int data[], int count)
{
    int i, j, m, mi;
    for (i = 0; i < count - 1; i++)
    {
        /* find the minimum */
        mi = i;
        for (j = i + 1; j < count; j++)
            if (data[j] < data[mi])
                mi = j;

        m = data[mi];

        /* move elements to the right */
        for (j = mi; j > i; j--)
            data[j] = data[j-1];

        data[i] = m;
    }
}

