func BubbleSort(list sort.Interface) {
    for itemCount := list.Len() - 1; ; itemCount-- {
        hasChanged := false;
        for current := 0; current < itemCount; current++ {
            next := current + 1
            if list.Less(next, current) {
                list.Swap(current, next)
                hasChanged = true
            }
        }
        if !hasChanged {
            break
        }
    }
}
