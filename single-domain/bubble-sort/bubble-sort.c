int main(void) {
  const int numberOfElements = 6;
  char arrayOfCharsToSort[numberOfElements];
  //Fill in the array with the unsorted data.
  arrayOfCharsToSort[0] = 'C';
  arrayOfCharsToSort[1] = 'A';
  arrayOfCharsToSort[2] = 'Z';
  arrayOfCharsToSort[3] = 'G';
  arrayOfCharsToSort[4] = 'S';
  arrayOfCharsToSort[5] = 'J';
  
  //Start sorting
  int i, j; //variables used for for loops.
  char temporaryCharHolder; //used to swap values of two array entries
  for (i = 0; i < numberOfElements; i++) {
    for (j = 0; j < numberOfElements - 1; j++) {
      if (arrayOfCharsToSort[j] > arrayOfCharsToSort[j + 1]) {
        temporaryCharHolder = arrayOfCharsToSort[j];
        arrayOfCharsToSort[j] = arrayOfCharsToSort[j + 1];
        arrayOfCharsToSort[j + 1] = temporaryCharHolder;
      }
    }
  }
  
  //Print out the sorted results
  for (i = 0; i < numberOfElements; i++) {
    printf("%c\n", arrayOfCharsToSort[i]);
  }
  
  return 0;
}

