#include <algorithm>
 
 template<typename Iterator>
 void bubbleSort(Iterator first, Iterator last)
 {
     Iterator i, j;
     for (i = first; i != last; i++)
         for (j = first; j < i; j++)
             if (*i < *j)
                 std::iter_swap(i, j); // or std::swap(*i, *j);
 }
 
 template<typename Iterator, class StrictWeakOrdering>
 void bubbleSort(Iterator first, Iterator last, StrictWeakOrdering compare)
 {
     Iterator i, j;
     for (i = first; i != last; i++)
         for (j = first; j < i; j++)
             if (compare(*i, *j))
                 std::iter_swap(i, j);
 }

template <typename Iterator>
void bubbleSort(Iterator first, Iterator last)
{
    for (Iterator i = first; i != last - 1; ++i)
        for (Iterator j = first; j != (last - 1 - distance(first, i)); ++j)
            if (*j > *(j + 1))
                std::swap(*j, *(j + 1));
}

template <typename Iterator, typename StrictWeakOrdering>
void bubbleSort(Iterator first, Iterator last, StrictWeakOrdering compare)
{
    for (Iterator i = first; i != last - 1; ++i)
        for (Iterator j = first; j != (last - 1 - distance(first, i)); ++j)
            if (compare(*j, *(j + 1)))
                std::swap(*j, *(j + 1));
}
