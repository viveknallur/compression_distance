use 5.010;
use warnings;
use strict;
use threads;
use IO::Handle; # for autoflush

use constant THREADS => 503;
# stack size may need tuning for your arch, default of 8MB is likely to not
# work well on 32 bit systems or those with limited memory.
use constant THREAD_STACK_SIZE => 512 * 1024;

my $passes = shift;
die "Usage: $0 [passes]\n"
  unless defined $passes && int($passes) > 0;
$passes = int($passes);

my(@pipes, @threads);

@pipes = map {
  pipe my($r, $w) or die "pipe() failed";
  { read => $r, write => $w }
} (0 .. THREADS-1);

@threads = map {
  my $in = $pipes[$_]{read};
  $in->autoflush;
  my $out = $pipes[($_ + 1) % THREADS]{write};
  $out->autoflush;
  my $thread_id = $_ + 1;
  threads->create
    ({ stack_size => THREAD_STACK_SIZE, },
     sub {           # $in, $out and $thread_id are captured in this closure
       while(my $msg = <$in>) { # receive message
         chomp $msg;
         if($msg eq 'EXIT') {   # asked to exit
           last;
         } elsif($msg > 0) {    # still work to do
           say $out --$msg;     # send message
         } else {               # no more work to do
           say $thread_id;      # output result
           # tell all threads to exit
           say $_ 'EXIT' foreach map { $_->{write} } @pipes;
           last;
         }
       }
     });
} (0 .. THREADS-1);

# inject initial message
my $start_fh = $pipes[0]{write};
say $start_fh $passes;

# collect exited threads
$_->join foreach @threads;

