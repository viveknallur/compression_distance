import collections
import csv
import fnmatch
import os


def collectFromCSV(csvfile, all_ncd):
    """
    Opens the csv file and reads data into 'all_ncd', row by row.
    all_ncd is assumed to be a dictionary of the type: {[list]: val, [list]:val}
    Each key in all_ncd is a list, containing the candidate and the thing
    it is being compared with
    """
    with open (csvfile, 'r') as csvf:
        data = csv.reader(csvf, delimiter = ',')
        num_rows = 0
        for row in data:
            candidate, comparison, score = row
            all_ncd[frozenset([candidate, comparison])] = float(score)
            num_rows += 1
        print("Number of rows read: %d"%(num_rows))
        return all_ncd

def get_distance(x,y, all_ncd):
    """
    Returns the pointwise distance between two items, 'x' and 'y' from the
    all_ncd dictionary. First, we loop through the enumeration of the keys
    and check if both 'x' and 'y' are in all_ncd, then return the value, else
    return False. Ideally, the function should never return False.
    """
    for key, value in all_ncd.items():
        if x in key and y in key:
            return value
        else:
            return False

def getFileList(rootdir, filePattern):
    FileList = [os.path.join(dirpath, f) \
            for dirpath, dirnames, files in os.walk(rootdir) \
            for f in fnmatch.filter(files, filePattern)]
    return FileList
    

if __name__ == "__main__":
    import sys
    if not len(sys.argv) > 1:
        dirPath = os.path.dirname(os.path.abspath(__file__))
        print(os.path.dirname(os.path.abspath(__file__)))
    else:
        dirPath = sys.argv[1]

    FileList = getFileList(dirPath, '*.csv')
    print("Number of files: %d"%(len(FileList)))
    all_ncd = collections.defaultdict(frozenset)
    for fil in FileList:
        all_ncd = collectFromCSV(fil, all_ncd)
        print("Number of keys found: %d"%(len(all_ncd)))
